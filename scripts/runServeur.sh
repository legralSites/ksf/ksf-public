#!/bin/sh
LOGDIR="/www/log/conferencier";
LOGSRV="${LOGDIR}/conferencier.log";
PROJETDIR="/www/projets/conferencier";
PROJETPATH="${PROJETDIR}/conferencier-serveur.js";

echo "*************************************************************************"
echo " demmarage de  $PROJETDIR/conferencier-serveur.js"
if [ ! -d $LOGDIR ]; then
  echo " repertoire de log: ${LOGDIR} n'existe pas -> creation";
  mkdir -p $LOGDIR
fi
echo "demarrer le: "  `date` >>  $LOGSRV
nohup node $PROJETPATH >> $LOGSRV  &


