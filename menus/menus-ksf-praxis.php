<?php
$ext='.html';
// ==== menu: primaire ==== //

$mn='ksf-praxis';
$pagePath=PAGESLOCALES_ROOT."/$mn/";

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,"$pagePath$p$ext");
        // -- parametrer la page -- //
        $m->setAttr($p,'visible',1);                		// 0: le li ne sera pas affiche 1:afficher
        $m->setAttr($p,'menuTitre',"les praxis");              	// afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
//        $m->setAttr($p,'menuTitle',"KSF");       		// afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($p,'titre',"les praxis");   		// titre de la page: afficher dans le bas de page
//      $m->setMeta($p,'title','tutoriels - accueil(meta)');   // meta <title> (si non definit title=titre)
//      $m->addCssA($p,'dossier1');                          // applique le style dossier1 a la balise <a>


$p='ksf-praxis-Dijon';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr("$p",'menuTitre','Dijon');
        $m->setAttr("$p",'titre',"praxis - Dijon");
        $m->addCssA("$p",'dossier1');

// - inclusion des sous menus - //      
include('./menus/menus-ksf-praxis-Dijon.php');

?>
