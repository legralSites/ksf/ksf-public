<?php
$ext='.html';

// ==== menu: obediences ==== //
$mn='ksf-obediences';
$pagePath=PAGESLOCALES_ROOT."/ksf-jeux/$mn/";

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        // -- parametrer la page -- //
        $m->setAttr($p,'visible',1);                   // 0: le li ne sera pas affiche 1:afficher
        $m->setAttr($p,'menuTitre','obédiences');               // afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
//        $m->setAttr($p,'menuTitle',$p);       // afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($p,'titre',$p);   // titre de la page: afficher dans le bas de page
//	$m->addCssA($p,'dossier1');


$p='ksf-obedience-Camarillia';
$m->addCallPage($p,"$pagePath$p$ext");
        $m->setAttr($p,'menuTitre','Camarillia');
        $m->setAttr($p,'titre',"$mn - $p");
        $m->addCssA($p,'dossier1');            // applique le style dossier1 a la balise <a>

$p='ksf-obedience-Sabbat';
//$m->addCallPage($p,$pagePath.$p.'-accueil'.$ext);
$m->addCallPage($p,"$pagePath$p$ext");
        $m->setAttr($p,'menuTitre','Sabbat');
        $m->setAttr($p,'titre',"$mn - $p");
        $m->addCssA($p,'dossier1');            // applique le style dossier1 a la balise <a>

$p='ksf-obedience-Anarch';
//$m->addCallPage($p,$pagePath.$p.'-accueil'.$ext);
$m->addCallPage($p,"$pagePath$p$ext");
        $m->setAttr($p,'menuTitre','Anarch');
        $m->setAttr($p,'titre',"$mn - $p");
	$m->addCssA($p,'dossier1');


// ==== menu: camarilla factions ==== //
$mn='ksf-obedience-Camarillia';
$p=$mn.'accueil';
//$m=$gestMenus->addMenu($mn,$p,"$pagePath$p$ext");
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        // -- parametrer la page -- //
        $m->setAttr($p,'visible',1);           // 0: le li ne sera pas affiche 1:afficher
        $m->setAttr($p,'menuTitre',$mn);       // afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
        $m->setAttr($p,'menuTitle',$mn);       // afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($p,'titre',$p);   // titre de la page: afficher dans le bas de page
//        $m->addCssA($p,'dossier1');

$p='Gangrel';
$m->addCallPage($p,"$pagePath".'faction-'.$p.$ext);
        $m->setAttr($p,'menuTitre',$p);
        $m->setAttr($p,'titre',"$mn - $p");
//        $m->addCssA($p,'dossier1');

$p='Brujah';
$m->addCallPage($p,"$pagePath".'faction-'.$p.$ext);
        $m->setAttr($p,'menuTitre',$p);
        $m->setAttr($p,'titre',"$mn - $p");
//      $m->addCssA($p,'dossier1');


$p='Malkavien';
$m->addCallPage($p,"$pagePath".'faction-'.$p.$ext);
        $m->setAttr($p,'menuTitre',$p);
        $m->setAttr($p,'titre',"$mn - $p");
//        $m->addCssA($p,'dossier1');

$p='Toreador';
$m->addCallPage($p,"$pagePath".'faction-'.$p.$ext);
        $m->setAttr($p,'menuTitre',$p);
        $m->setAttr($p,'titre',"$mn - $p");
//        $m->addCssA($p,'dossier1');

$p='Ventrue';
$m->addCallPage($p,"$pagePath".'faction-'.$p.$ext);
        $m->setAttr($p,'menuTitre',$p);
        $m->setAttr($p,'titre',"$mn - $p");
//        $m->addCssA($p,'dossier1');

$p='Nosferatu';
$m->addCallPage($p,"$pagePath".'faction-'.$p.$ext);
        $m->setAttr($p,'menuTitre',$p);
        $m->setAttr($p,'titre',"$mn - $p");
//        $m->addCssA($p,'dossier1');


$p='Tremere';
$m->addCallPage($p,"$pagePath".'faction-'.$p.$ext);
        $m->setAttr($p,'menuTitre',$p);
        $m->setAttr($p,'titre',"$mn - $p");
//        $m->addCssA($p,'dossier1');


// ==== menu: Sabbat factions ==== //
$mn='ksf-obedience-Sabbat';
$p=$mn.'accueil';
$m=$gestMenus->addMenu($mn,$p,$pagePath."$p.html");
        // -- parametrer la page -- //
        $m->setAttr($p,'visible',1);           // 0: le li ne sera pas affiche 1:afficher
        $m->setAttr($p,'menuTitre',$mn);       // afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
        $m->setAttr($p,'menuTitle',$mn);       // afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($p,'titre',$p);   // titre de la page: afficher dans le bas de page
//        $m->addCssA($p,'dossier1');

$p='Lasombra';
$m->addCallPage($p,$pagePath.'faction-'.$p.$ext);
        $m->setAttr($p,'menuTitre',$p);
        $m->setAttr($p,'titre',"$mn - $p");
//        $m->addCssA($p,'dossier1');

$p='Tzimisce';
$m->addCallPage($p,$pagePath.'faction-'.$p.$ext);
        $m->setAttr($p,'menuTitre',$p);
        $m->setAttr($p,'titre',"$mn - $p");
//        $m->addCssA($p,'dossier1');

// ==== menu: Anarch factions ==== //
$mn='ksf-obedience-Anarch';
$p='accueil';
$m=$gestMenus->addMenu($mn,$p,"$pagePath$p$ext");
        // -- parametrer la page -- //
        $m->setAttr($p,'visible',1);           // 0: le li ne sera pas affiche 1:afficher
        $m->setAttr($p,'menuTitre',$mn);       // afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
        $m->setAttr($p,'menuTitle',$mn);       // afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($p,'titre',$p);   // titre de la page: afficher dans le bas de page
//        $m->addCssA($p,'dossier1');

$p='Assamite';
$m->addCallPage($p,$pagePath.'faction-'.$p.$ext);
        $m->setAttr($p,'menuTitre',$p);
        $m->setAttr($p,'titre',"$mn - $p");
//        $m->addCssA($p,'dossier1');

$p='Giovanni';
$m->addCallPage($p,$pagePath.'faction-'.$p.$ext);
        $m->setAttr($p,'menuTitre',$p);
        $m->setAttr($p,'titre',"$mn - $p");
//        $m->addCssA($p,'dossier1');

$p='Ravnos';
$m->addCallPage($p,$pagePath.'faction-'.$p.$ext);
        $m->setAttr($p,'menuTitre',$p);
        $m->setAttr($p,'titre',"$mn - $p");
//        $m->addCssA($p,'dossier1');


$p='Sethite';
$m->addCallPage($p,$pagePath.'faction-'.$p.$ext);
        $m->setAttr($p,'menuTitre',$p);
        $m->setAttr($p,'titre',"$mn - $p");
//        $m->addCssA($p,'dossier1');


// ==== menu: Anarch factions ==== //
$pagePath0=$pagePath;	// sauvegarde du repertoire local

//$pagePath.='cgp/';
$pagePath=PAGESLOCALES_ROOT."cgp/";

$mn='ksf-cartoGralPhie';
$p=$mn.'-accueil';
$m=$gestMenus->addMenu($mn,$p,"$pagePath$p$ext");
        // -- parametrer la page -- //
        $m->setAttr($p,'visible',1);           // 0: le li ne sera pas affiche 1:afficher
        $m->setAttr($p,'menuTitre',$mn);       // afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
        $m->setAttr($p,'menuTitle',$mn);       // afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($p,'titre',$p);   // titre de la page: afficher dans le bas de page
//        $m->addCssA($p,'dossier1');

$p='contexte';
$m->addCallPage($p,$pagePath.$p.$ext);
        $m->setAttr($p,'menuTitre',$p);
        $m->setAttr($p,'titre',"$mn - $p");
//        $m->addCssA($p,'dossier1');

$pagePath=$pagePath0;	// restauration du repertoire local
unset ($pagePath0);

 

?>
