<?php
$ext='.html';
// ==== menu: primaire ==== //

$mn='ksf';
$pagePath=PAGESLOCALES_ROOT."/";

$p='ksf-asso';
$m=$gestMenus->addMenu($mn,$p,$pagePath.$p.'/accueil.html');
        // -- parametrer la page -- //
        $m->setAttr($p,'visible',1);                		// 0: le li ne sera pas affiche 1:afficher
        $m->setAttr($p,'menuTitre',"KSF");              	// afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
        $m->setAttr($p,'menuTitle',"KSF");       		// afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($p,'titre',"l'association KSF");   		// titre de la page: afficher dans le bas de page
//      $m->setMeta($p,'title','tutoriels - accueil(meta)');   // meta <title> (si non definit title=titre)
//      $m->addCssA($p,'dossier1');                          // applique le style dossier1 a la balise <a>


$p='ksf-jeux';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr("$p",'menuTitre','le jeux');
        $m->setAttr("$p",'titre',"le système de jeux");
        $m->addCssA("$p",'dossier1');
	
$p='ksf-praxis';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr("$p",'menuTitre','Praxis');
        $m->setAttr("$p",'titre',"les Praxis");
        $m->addCssA("$p",'dossier1');

$p='ksf-tr';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr("$p",'menuTitre',"l'appli");
        $m->setAttr("$p",'titre',"l'application Temps réel");
        $m->addCssA("$p",'dossier1');

$p='ksf-admins';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr("$p",'menuTitre',"admin");
        $m->setAttr("$p",'titre',"administrations");
        $m->addCssA("$p",'dossier1');



// - inclusion des sous menus - //      
include('./menus/menus-ksf-asso.php');
include('./menus/menus-ksf-jeux.php');
include('./menus/menus-ksf-praxis.php');
include('./menus/menus-ksf-tr.php');
include('./menus/menus-ksf-admins.php');


?>
