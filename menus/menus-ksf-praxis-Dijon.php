<?php
$ext='.html';
// ==== menu: ksf-praxis-Dijon ==== //

$mn='ksf-praxis-Dijon';
$pagePath=PAGESLOCALES_ROOT."/ksf-praxis/$mn/";

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,"$pagePath$p$ext");
        // -- parametrer la page -- //
        $m->setAttr($p,'visible',1);                		// 0: le li ne sera pas affiche 1:afficher
        $m->setAttr($p,'menuTitre',"Dijon");              	// afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
//        $m->setAttr($p,'menuTitle',"KSF");       		// afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($p,'titre',"la praxis de Dijon");   	// titre de la page: afficher dans le bas de page
//      $m->setMeta($p,'title','tutoriels - accueil(meta)');   // meta <title> (si non definit title=titre)
//      $m->addCssA($p,'dossier1');                          // applique le style dossier1 a la balise <a>

/*
$p='ksf-regles';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr("$p",'menuTitre','règles');
        $m->setAttr("$p",'titre',"$mn - $p");
        $m->addCssA("$p",'dossier1');
*/

$p='ksf-cartoGralPhie';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr("$p",'menuTitre',$p);
        $m->setAttr("$p",'titre',"$mn - $p");
        $m->addCssA("$p",'dossier1');

$p='ksf-scenaristiques';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
	$m->setAttr("$p",'menuTitre','scénaristiques');
        $m->setAttr("$p",'titre',"$mn - $p");
        $m->addCssA("$p",'dossier1');


// - inclusion des sous menus - //      
//include('./menus/menus-obediences.php');
?>
