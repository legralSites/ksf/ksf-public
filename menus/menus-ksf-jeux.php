<?php
$ext='.html';
// ==== menu: primaire ==== //

$mn='ksf-jeux';
$pagePath=PAGESLOCALES_ROOT."/$mn/";

$p='accueil';
$m=$gestMenus->addMenu($mn,$p,"$pagePath$p$ext");
        // -- parametrer la page -- //
        $m->setAttr($p,'visible',1);                		// 0: le li ne sera pas affiche 1:afficher
        $m->setAttr($p,'menuTitre',"le Jeu");              	// afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
//        $m->setAttr($p,'menuTitle',"KSF");       		// afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($p,'titre',"le système de jeux");   	// titre de la page: afficher dans le bas de page
//      $m->setMeta($p,'title','tutoriels - accueil(meta)');   // meta <title> (si non definit title=titre)
//      $m->addCssA($p,'dossier1');                          // applique le style dossier1 a la balise <a>


$p='ksf-regles';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr("$p",'menuTitre','règles');
        $m->setAttr("$p",'titre',"$mn - $p");
        $m->addCssA("$p",'dossier1');


$p='ksf-obediences';
$m->addCallPage($p,$pagePath.$p.'/accueil.html');
        $m->setAttr("$p",'menuTitre',"les obédiences");
        $m->setAttr("$p",'titre'," les obédiences");
        $m->addCssA("$p",'dossier1');




$p='lexiques';
$m->addCallPage($p,"$pagePath$p$ext");
        $m->setAttr($p,'menuTitre',$p);
        $m->setAttr($p,'titre',"$mn - $p");
//        $m->addCssA($p,'dossier1');


// - inclusion des sous menus - //      
include('./menus/menus-obediences.php');
include('./menus/menus-scenaristiques.php');
include('./menus/menus-regles.php');

?>
