/*
données temporel pour dijonPraxis
date de creation: 2014.08.31
date de modification:2014.08.31
auteur: pascal TOLEDO pascal.toledo - legral.fr
*/

// - clans - //
vampireEvt=[];evtNo=0;
vampireEvt[evtNo++]={"date_deb":{"an":1493,"ms":10,"jr":23},"titre":"convention des épines","title":"convention des épines","texte":"convention des épines"};
vampireEvt[evtNo++]={"date_deb":{"an":1917},"titre":"Les Brujah domine grace à la révolution russe"};
vampireEvt[evtNo++]={"date_deb":{"an":1934},"date_fin":{"an":1936},"titre":"guerre civile espagole","title":"La camarilla renforce son contrôle su l'europe de l'Ouest","texte":""};


GangrelEvt=[];evtNo=0;
GangrelEvt[evtNo++]={"date_deb":{"an":2000,"ms":1,"jr":1},'date_fin':{"an":2010,"ms":2,"jr":7},"titre":"gangrelTest1","title":"g t1","texte":""};

SabbatEvt=[];evtNo=0;

AnarchEvt=[];evtNo=0;
AnarchEvt[evtNo++]={"date_deb":{"an":2014,"ms":5,"jr":17},"titre":"","title":"message de Alphonse SARLANT pour Paul LAVALLE","texte":""};
AnarchEvt[evtNo++]={"date_deb":{"an":2014,"ms":5,"jr":22},"titre":"","title":"message de Thomas (INGBART) pour Paul LAVALLE","texte":""};
AnarchEvt[evtNo++]={"date_deb":{"an":2014,"ms":5,"jr":26},"titre":"","title":"message de SILIIS pour inconnu","texte":""};


// - villes - //

DijonEvt=[];evtNo=0;


LosAngelesEvt=[];evtNo=0;
LosAngelesEvt[evtNo++]={"date_deb":{"an":1944},"titre":"","title":"gouvernée par Don Sebastian Rodriguez, un prince Toreador arrogant. Le Brujah Jeremy McNeil demanda justice, Sebastian le fit battre jusqu'à la limite d ela torpeur","texte":""};
LosAngelesEvt[evtNo++]={"date_deb":{"an":1944,"ms":12,"jr":21},"titre":"","title":"McNeil et son proche Salvador Garcia obédikence(?) lancèrent une vaste offensive contre le refuge des Anciens. Un Archonte devait arrivé mais n'arriva jamais (tué?).","texte":""};
LosAngelesEvt[evtNo++]={"date_deb":{"an":1945,"ms":1,"jr":7},"titre":"","title":"les derniers resistants abdiquerent, la cité Anarch Libre de L.A. fut proclamée","texte":""};
//LosAngelesEvt[evtNo++]={"date_deb":{"an":1944},"titre":"","title":"","texte":""};

LondreEvt=[];evtNo=0;

BerlinEvt=[];evtNo=0;

DijonEvt=[];evtNo=0;
// parchemin du sacré coeur
DijonEvt[evtNo++]={"date_deb":{"an":2014,"ms":1,"jr":6},"titre":"","title":"Karl (SHRECK justicar Tréméré?) est capturé par les Garous.","texte":""};
DijonEvt[evtNo++]={"date_deb":{"an":2014,"ms":2,"jr":17},"titre":"","title":"Le dix-septième jour du moi de février de l'an 2014. Cela fait maintenant six semaines que Karl a été capturé par les Garous. Jergen a déclaré ce jour sa destruction auprès du Baron  . Nul n'est dupe sur son sort, et nous devons désormais compter avec le fait que les garous savent tout de notre projet. Esperrons qu'ils ne retrouveront pas le sang avant nous.","texte":""};
DijonEvt[evtNo++]={"date_deb":{"an":2014,"ms":4,"jr":21},"titre":"","title":"Le vingt et un du mois d'avril de l'an 2014. Grâce en soit rendue au père Vernian, nous avons fini par le retrouver, le sang du Traître. Il est dans le caveau, avec cette chose. Reste à pratiquer le rituel. Il nous a ouvert les grilles , mais nous ne voulons pas commettre d'erreur. Nous allons prendre notre temps. Et pour commencer, nous allons déplacer notre fondation pour avoir tout notre matériel à disposition. ","texte":""};
DijonEvt[evtNo++]={"date_deb":{"an":2014,"ms":4,"jr":26},"titre":"","title":"Le vingt-sixieme jour du mois d'avril de l'an 2014. Notre primogène m'a envoyée auprès des Anciens pour que je me renseigne sur leur découverte, mais Sarlant aurait déjà confié ses trouvailles, apparemment trois anneaux, aux rats d'égoûts. \
C'est tout de meme étrange que cela se produise seulement quelques jours apres que nous ayons réveillés cette chose. Je pense que les deux sont connectés. Sont-ce trois maillons du Necromant vivant? Quel sombre et maléfique secret va-t-on encore découvrir sur ce mémorable fugitif..","texte":""};
DijonEvt[evtNo++]={"date_deb":{"an":2014,"ms":5,"jr":1},"titre":"","title":"Le premier jours du mois de mai de l'an 2014. J'ai entendu le père parler tout seul. Il mentionnait un certain Gaebolg. Je ne sais pas plus comment il s'appelle. Comme il a vu que je l'avais surpris, il s'est arrété brusquement. Bien que je n'ai jamais eu de raison de me défier de lui, je me demande si mon Primogène a eu raison de continuer à lui faire confiance. Avions-nous vraiment encore besoin de lui? Je commence à le trouver un peu bizarre. ","texte":""};
DijonEvt[evtNo++]={"date_deb":{"an":2014,"ms":5,"jr":12},"titre":"","title":"Le douzieme jour du mois de mai de l'an 2014. Aujourd'hui, nous allons accomplir le rituel. Nous apporterons le sang. Jezabel Jezen prononcera la formule. Quand à moi, je serai le centre du cercle, la clé, celle dont le sang provoquera l'ouverture. Ceci sera peut-etre la fin de presque sept siecle de traque. Vernian est resté à l'interieur. Je préfère qu'il en soit ainsi. Depuis le crepuscule, il est plus étrange et intenable que jamais.","texte":""};
DijonEvt[evtNo++]={"date_deb":{"an":2014,"ms":5,"jr":13},"titre":"","title":"Je t'ai doucement réveillée.\
Ta peau a la blancheur des anges. \
Tes yeux ouvrent sur des horizons infinis et  inssondables. \
Je te touches, tu frémis, frissonnes comme une feuille d\'automne sous la froideur de ma carresse. \
Je  t\'aime, tu es mon inspiration, ta présence m\'est rémission /obsession?passion?","texte":""};

DijonEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":4},"titre":"","title":"Nous avons l’honneur de vous annoncer que  la Fondation « Sauvez Willy » vient tout juste de voir le jour. Elle est le fruit de la réflexion de plusieurs Caïnites d’horizons et de bords différents, tous sensibilisés autour d’un même problème, d’une  même cause. \
	Cette fondation agit principalement dans  trois domaines : l’aide aux Caïnites vulnérables, le développement de la connaissance et la sensibilisation à l’Art. Elle favorise également le  développement de la philanthropie.\
Nous sommes aussi heureux de vous  annoncer la mise en place de notre première action caritative. Celle qui  nous touche le plus, celle qui a donné le nom à notre fondation. Et  c’est avec joie que nous tendons la main à M. William Drake.\
Nous avons hélas appris la réelle difficulté du clan Ventrue a s’impliquer dans les affaires culturelles de notre chère Praxis. Mais nous allons réagir tous ensemble et Sauver Willy!\
Vous voulez en savoir plus, écrivez nous : fondation.sauvezwilly.2014@gmail.com\
Mentions légales : fondation  gouvernée en co-présidence, financée par mécénat, capitaux propres détenus à 50% par la SFVC et à 50% par un donateur protégé par  l’anonymat.","texte":""};



// - personnages - //

// - personnages - gangrel - //

Gaebolg=[];evtNo=0;


ShafBrunkerEvt=[];evtNo=0;
//ShafBrunkerEvt[evtNo++]={"date_deb":{"an":0,"ms":1,"jr":1},"titre":"","title":"","texte":""};
ShafBrunkerEvt[evtNo++]={"date_deb":{"an":1930/*,"ms":1,"jr":1*/},"titre":"etreinte","title":"","texte":"etreinte"};

MatthewMacFersonEvt=[];evtNo=0;
MatthewMacFersonEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":24},"titre":"","title":"enlevé par les garous","texte":""};


// - Tréméré - //
JergenHassenweldEvt=[];evtNo=0;
JergenHassenweldEvt[evtNo]={"date_deb":{"an":2014,"ms":5,"jr":11},"titre":"","title":"Cher Massimiliano,<br>\
Votre prévenance me va droit au cœur,  et je vous garantis que nous prendrons grand soin de ne pas éveiller l’attention des Garous en nous installant sur leur territoire. Mon  projet vous paraît certainement insensé, mais le cadre seul de notre nouvelle demeure justifie entièrement que l’on prenne quelque risque.  Vous comprendrez toutefois que je ne puisse vous révéler quoi que ce soit de nos intentions réelles. Je ne doute pas de la sincérité de votre  amitié, mais nous nous connaissons depuis trop peu de temps pour que je  vous dévoile chacun de mes secrets. Quant à vos interrogations concernant les carnets d’Ambrogino Giovanni, je verrai ce que je peux  faire, mais il est peu probable que je puisse découvrir quoi que ce soit sur une personne que l’on dit disparue depuis maintenant près de cinq siècles.<br>\
Votre ami,<br>\
Jergen Hassenweld"};

// - personnages - Anarch - //
PaulLavaleeEvt=[];evtNo=0;
PaulLavaleeEvt[evtNo++]={"date_deb":{"an":2014,"ms":5,"jr":22},"titre":"","title":"message sur repondeur de Thomas (Ingbart)","texte":""};

AlphonseSarlantEvt=[];evtNo=0;
AlphonseSarlantEvt[evtNo++]={"date_deb":{"an":2014,"ms":5,"jr":17},"titre":"","title":"17 mai 2014.Message téléphonique pour Paul Lavalee: Bon, chacun d'entre vous est au courant qu'il se passe des trucs pas nets depuis quelques temps. Pour l'ensemble, vous semblez y voir une sorte d'effet secondaire de notre découverte. Pas moi. Moi je pense que ces cons de sorciers ont encore  frappé. Voyez le tableau : des vieux trucs qui datent de Mathusalem refont surface, on les prive de récréation en confiant le bébé à d'autres, là-dessus ils deviennent tout rouges et commencent à comploter pour qu'on leur rende leur sucette. Le plan tourne ricard sans flotte et on en arrive où on en arrive. Pour prouver ce que je dis, j'ai envoyé Nasim faire un tour du côté de leur fondation quartier Chambre de Commerce : plus un rat. Je vous le dis, il y a du louche. Vous le savez comme moi, ceux-là ne sont pas franchement comme les autres de leur race. Sérieux, ils n'ont jamais foutu un pied dans aucun laboratoire ni dans aucune fac de la ville. Tout ce qui les branche c'est de rester entre eux à faire les mêmes trucs secrets depuis des décennies. M'est avis que ce petit côté incestueux cache autre chose : une mission précise, que je ne serais pas étonné de voir liée à ces babioles qu'on a retrouvées. Alors voilà ce que je préconise : on se met tous en ordre de bataille pour trouver où ils ont été se planquer et on les fait parler. On commence par chez nous : Le Parc, les allées, et un petit tour vers les Tanneries. Bonne chasse!","texte":""};

NasimBadraniEvt=[];evtNo=0;
AnneFretetEvt=[];evtNo=0;
ThomasIngbartEvt=[];evtNo=0;
GillianJohnsonEvt=[];evtNo=0;
CharlesTurgotEvt=[];evtNo=0;


Evt=[];evtNo=0;
Evt=[];evtNo=0;
Evt=[];evtNo=0;
Evt=[];evtNo=0;
Evt=[];evtNo=0;
Evt=[];evtNo=0;
Evt=[];evtNo=0;

// - les articles parus - //
ArticlesEvt=[];evtNo=0;
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":9,"jr":9},"titre":"la reconquete","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":9,"jr":23},"titre":"Recherche de la fondation tréméré","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":9,"jr":24},"titre":"Retour aux affaires","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":9,"jr":28},"titre":"La mairie interdit une Manifestation","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":9,"jr":30},"titre":"Recherche de la fondation tréméré-complément 1","title":"","texte":""};

ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":1},"titre":"Un juge disparait","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":2},"titre":"Début de Vendetta?","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":4},"titre":"Sauvez Willy!","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":5},"titre":"Heurts aux Tanneries","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":6},"titre":"Instabilité politique","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":7},"titre":"Malchance ou traitrise","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":8},"titre":"Réalité augmenté","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":10},"titre":"Bibliothéque Giovanni","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":11},"titre":"une cartographie se dessine","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":12},"titre":"Acte1-rapport","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":15},"titre":"Mort tragique","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":18},"titre":"Troubles dans l'est","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":19},"titre":"Un braquage pas d'enquete","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":19},"titre":"Le 15 septembre 1989","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":22},"titre":"Mariage avec la mort","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":22},"titre":"Les Procés Mithras","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":24},"titre":"Disparition de McFerson","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":25},"titre":"Startégie commune","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":26},"titre":"La Lettre Inachevée","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":28},"titre":"Elysium intermediaire","title":"Positionnement des goules pour contrer les factions ennemenis à la praxis","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":31},"titre":"La Lettre Inachevée","title":"Ouverture de la porte par la femme du soldat","texte":""};

ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":29},"titre":"Battue pour le petit Kévin","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":29},"titre":"Nos adeversaire s'organise","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":30},"titre":"Les assamites reviennent du Caern","title":"","texte":""};
ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":31},"titre":"l'Archonte disparu...","title":"","texte":""};
//ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":1},"titre":"","title":"","texte":""};
//ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":1},"titre":"","title":"","texte":""};
//ArticlesEvt[evtNo++]={"date_deb":{"an":2014,"ms":10,"jr":1},"titre":"","title":"","texte":""};































