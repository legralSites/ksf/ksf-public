/*
données temporel pour dijonPraxisChapitre, lieu ou se trouve la terre du millieu
date de creation: 2014.08.31
date de modification:2014.10.26
auteur: pascal TOLEDO pascal.toledo - legral.fr
*/
dijonPraxisChapitre=[];
dijonPraxisEvt=[];
evtNo=0;
// chapitre1 - scenario [1..5 - semaine 1..3-4]
dijonPraxisChapitre[1]=[];
dijonPraxisChapitre[1][0]={"date_deb":{"unite":"jr","an":2014,"ms":9,"jr":19},'date_fin':{"unite":"jr","an":2015,"ms":2,"jr":7},"titre":"chapitre1","title":"","texte":"chapitre 1"};	// timeline du chapitre

dijonPraxisEvt[evtNo++]={"date_deb":{"unite":"jr","an":2014,"ms":9,"jr":19,"hr":19,"mn":0},"titre":"1er réunion RP","title":"","texte":""};	// 1er réunion RP

dijonPraxisChapitre[1][1]=[]; // chapitre - scenario contener

dijonPraxisChapitre[1][1][0]={"date_deb":{"unite":"jr","an":2014,"ms":9,"jr":22},'date_fin':{"unite":"jr","an":2014,"ms":10,"jr":11},"titre":"acte 1","title":"trouver la fondation tréméré","texte":"acte1"}; // timeline de l'acte
dijonPraxisChapitre[1][1][1]={"date_deb":{"unite":"jr","an":2014,"ms":9,"jr":22},'date_fin':{"unite":"jr","an":2014,"ms":9,"jr":28},"titre":"s 1","title":"","texte":"s 1"}; // timeline de la semaine
dijonPraxisChapitre[1][1][2]={"date_deb":{"unite":"jr","an":2014,"ms":9,"jr":29},'date_fin':{"unite":"jr","an":2014,"ms":10,"jr":5},"titre":"s 2","title":"","texte":"s 2"};
dijonPraxisChapitre[1][1][3]={"date_deb":{"unite":"jr","an":2014,"ms":10,"jr":6},'date_fin':{"unite":"jr","an":2014,"ms":10,"jr":11},"titre":"s 3","title":"","texte":"s 3"};


dijonPraxisChapitre[1][2]=[];
dijonPraxisChapitre[1][2][0]={"date_deb":{"unite":"jr","an":2014,"ms":10,"jr":20},'date_fin':{"unite":"jr","an":2014,"ms":11,"jr":8},"titre":"acte 2","title":"","texte":"acte 2"}; // acte 2
dijonPraxisChapitre[1][2][1]={"date_deb":{"unite":"jr","an":2014,"ms":10,"jr":20},'date_fin':{"unite":"jr","an":2014,"ms":10,"jr":26},"titre":"s 1","title":"","texte":"s 1"}; // semaine
dijonPraxisChapitre[1][2][2]={"date_deb":{"unite":"jr","an":2014,"ms":10,"jr":27},'date_fin':{"unite":"jr","an":2014,"ms":11,"jr":2},"titre":"s 2","title":"","texte":"s 2"};
dijonPraxisChapitre[1][2][3]={"date_deb":{"unite":"jr","an":2014,"ms":11,"jr":3},'date_fin':{"unite":"jr","an":2014,"ms":11,"jr":8},"titre":"s 3","title":"","texte":"s 1"};

dijonPraxisChapitre[1][3]=[];
dijonPraxisChapitre[1][3][0]={"date_deb":{"unite":"jr","an":2014,"ms":11,"jr":17},'date_fin':{"unite":"jr","an":2014,"ms":12,"jr":13},"titre":"acte 3","title":"","texte":"acte 3"};
dijonPraxisChapitre[1][3][1]={"date_deb":{"unite":"jr","an":2014,"ms":11,"jr":17},'date_fin':{"unite":"jr","an":2014,"ms":11,"jr":23},"titre":"s 1","title":"","texte":"s 1"};
dijonPraxisChapitre[1][3][2]={"date_deb":{"unite":"jr","an":2014,"ms":11,"jr":24},'date_fin':{"unite":"jr","an":2014,"ms":11,"jr":30},"titre":"s 2","title":"","texte":"s 2"};
dijonPraxisChapitre[1][3][3]={"date_deb":{"unite":"jr","an":2014,"ms":12,"jr":1},'date_fin':{"unite":"jr","an":2014,"ms":12,"jr":7},"titre":"s 3","title":"","texte":"s 3"};
dijonPraxisChapitre[1][3][4]={"date_deb":{"unite":"jr","an":2014,"ms":12,"jr":8},'date_fin':{"unite":"jr","an":2014,"ms":12,"jr":13},"titre":"s 4","title":"","texte":"s 4"};


dijonPraxisChapitre[1][4]=[];
dijonPraxisChapitre[1][4][0]={"date_deb":{"unite":"jr","an":2014,"ms":12,"jr":22},'date_fin':{"unite":"jr","an":2015,"ms":1,"jr":10},"titre":"acte 3","title":"","texte":"acte 4"};
dijonPraxisChapitre[1][4][1]={"date_deb":{"unite":"jr","an":2014,"ms":12,"jr":22},'date_fin':{"unite":"jr","an":2014,"ms":12,"jr":28},"titre":"s 1","title":"","texte":"s 1"};
dijonPraxisChapitre[1][4][2]={"date_deb":{"unite":"jr","an":2014,"ms":12,"jr":29},'date_fin':{"unite":"jr","an":2015,"ms":1,"jr":4},"titre":"s 2","title":"","texte":"s 2"};
dijonPraxisChapitre[1][4][3]={"date_deb":{"unite":"jr","an":2015,"ms":1,"jr":5},'date_fin':{"unite":"jr","an":2015,"ms":1,"jr":10},"titre":"s 3","title":"","texte":"s 3"};


dijonPraxisChapitre[1][5]=[];
dijonPraxisChapitre[1][5][0]={"date_deb":{"unite":"jr","an":2015,"ms":1,"jr":19},'date_fin':{"unite":"jr","an":2015,"ms":2,"jr":7},"titre":"acte 5","title":"","texte":"acte 5"};
dijonPraxisChapitre[1][5][1]={"date_deb":{"unite":"jr","an":2015,"ms":1,"jr":19},'date_fin':{"unite":"jr","an":2015,"ms":1,"jr":25},"titre":"s 1","title":"","texte":"s 1"};
dijonPraxisChapitre[1][5][2]={"date_deb":{"unite":"jr","an":2015,"ms":1,"jr":26},'date_fin':{"unite":"jr","an":2015,"ms":2,"jr":1},"titre":"s 2","title":"","texte":"s 2"};
dijonPraxisChapitre[1][5][3]={"date_deb":{"unite":"jr","an":2015,"ms":2,"jr":2},'date_fin':{"unite":"jr","an":2015,"ms":2,"jr":7},"titre":"s 3","title":"","texte":"s 3"};

/*
for(i=0;i<dijonPraxisChapitre[3].length;i++){
        dijonPraxisChapitre[3][i].titre= dijonPraxisChapitre[3][i].texte;
//        dijonPraxisChapitre[3][i].title= dijonPraxisChapitre[3][i].texte;
        }
*/
