/*!
*/

friseChapitre=null;
function createFriseChapitre()
{
friseChapitre=new Tfrise({
	'idHTML':'friseChapitre_support'
	,'supportRegles':'friseChapitre_supportRegles'
	,'supportDatas':'friseChapitre_supportDatas'
	,'bandeModele':{'date_deb': {"an": 2014,"ms":9,"jr":18,'unite':'jr'},"date_fin": {"an": 2015,"ms":2,"jr":8,'unite':'jr'},'left_marge': 0,'longueur': 1000,'top': 0,'hauteur': 10}
	});

friseChapitre.styleInit=function()
	{
	// -- initialisation des couleurs -- //
//	this.evtStyle=new Array();
//	this.evtStyle.backgroundColor=new Array();
	// == superEons == //
	// == Eons == //
//	this.evtStyle.backgroundColor['Hadeen']='#702';
//	this.evtStyle.backgroundColor['Archeen']='#F22';
//	this.evtStyle.backgroundColor['Proterozoique']='#F55';
//	this.evtStyle.backgroundColor['Phanerozoique']='#F99';
	}


friseChapitre.auto=function()
	{
	this.creer_regleA();
//	this.creerReperesAuto({'callbacks':{'texte':'Million','title':'details'}});
	this.creerReperesAuto({"repereNb":5});

	this.creer_bandeChapitres();
	this.creer_bandeActes();
	this.creer_bandeSemaines();

	this.showEvt_Chapitres();
	this.showEvt_Actes();
	this.showEvt_Semaines();
	this.showEvt();
	this.showNow();

//	this.creer_bandeSuperEons();
//	this.showEvt_Niveau('superEons',this.gestBandes.bandesData['superEons']);

	}

friseChapitre.creer_regleA=function()
	{
	var options=undefined;
	this.gestBandes.creerBande({"genre": "regle",idHTML_parent:this.supportRegles, "idHTML": "regleA","nom":"regleA","top":0,"hauteur":30,"title": ""},options);
	}

friseChapitre.creer_bandeChapitres=function()
	{
	var options=undefined;
	this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeChapitres","nom":"Chapitres","top": 0,"hauteur":10,"title": "duree des chapitres"},options);
	}

friseChapitre.creer_bandeActes=function()
	{
	var options={"opacity":0.75};
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeActes','nom':'Actes',"top": 10,'hauteur':10,"title": "période inter acte"},options);
	}

friseChapitre.creer_bandeSemaines=function()
	{
	var options=undefined;
	this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeSemaines","nom":"Semaines","top": 20,"hauteur":10,"title": "duree des semaines"},options);
	}



	// == function d'ajout des evt ==//
friseChapitre.showEvt_Niveau=function(niveau,bandeRef)
	{
	showActivity("ajoute un groupe d'evt",1);
	var t=eresGeologique[niveau];
	if(!t)return;
	if(typeof(bandeRef)!=='object'){showActivity("bandeRef n'est pas un objet!",1);return;}

	var global_Tfrise_msg='ajout de des eres dans la bande '+bandeRef.idHTML;
	for(var p in t)
		{
		if(typeof(t[p])=='object')
			{
			var out=t;
			out=p;
			var EvtNu=bandeRef.evtAdd(
				t[p]
				,{'classe': 'evt_'+niveau,'opacity': 1,'zIndex': 1005}
				,{
					 deb: function(t){showActivity(global_Tfrise_msg,'add')}
					,fin: function(t){showActivity(global_Tfrise_msg,'add')}
				 }
				);
			// == application du style personnel a l'evt == //
			var attr={};
			attr.texte=p;
			if(this.evtStyle.backgroundColor[p])attr.backgroundColor=this.evtStyle.backgroundColor[p];
			attr.click=function()
			 	{
			 	alert('click sur evt:'+this.innerHTML,1);
			 	}
			attr.dblclick=function()
		 		{
		 		alert('DOUBLE click sur repere:'+this.innerHTML,1);
		 		}
			this.gestBandes.bandesData[bandeRef.nom].evt[EvtNu].setAttrCSS(attr);
			}
		}
	}

friseChapitre.showEvt_Chapitres=function()
	{
	var bandeRef=this.gestBandes.bandesData.Chapitres;
	var EvtNu=bandeRef.evtAdd(
				dijonPraxisChapitre[1][0]
/*				,{'classe': 'evt_'+niveau,'opacity': 1,'zIndex': 1005}*/
				,{
//					 deb: function(t){showActivity(global_Tfrise_msg,'add')}
//					,fin: function(t){showActivity(global_Tfrise_msg,'add')}
				 }
				);
	}

friseChapitre.showEvt_Actes=function()
	{
	var bandeRef=this.gestBandes.bandesData.Actes;
	for (var scNu=1;scNu<6;scNu++){
		var EvtNu=bandeRef.evtAdd(
			dijonPraxisChapitre[1][scNu][0]
			,{"classe": "evt_acte", "opacity": 1,"zIndex": 1005}
			,{
//				 deb: function(t){showActivity(global_Tfrise_msg,'add')}
//				,fin: function(t){showActivity(global_Tfrise_msg,'add')}
			 }
			);
		}
	}
friseChapitre.showEvt_Semaines=function()
     {
     var bandeRef=this.gestBandes.bandesData.Semaines;
     var scNu=1,smNu=1;
     var c=dijonPraxisChapitre[1];
     for (var scNu=1;scNu<6;scNu++)
     for (var smNu=1;smNu<5;smNu++){
	  if(typeof(c[scNu][smNu])!=='object')continue;
	  var evtNu=bandeRef.evtAdd(
	       c[scNu][smNu]
	       ,{"classe": "evt_semaine","opacity": 1,"zIndex": 1005}
	       ,{
//	            deb: function(t){showActivity(global_Tfrise_msg,'add')}
//		    ,fin: function(t){showActivity(global_Tfrise_msg,'add')}
	       }
	       );

	// --- ajout des clicks (faire des modif dans timelne.js --- //
/*	var attr={};
	attr.click=function(e,scNu,smNu){
		this.limite(c[scNu][smNu].deb,c[scNu][smNu].fin);
		}
	bandeRef.evt[evtNu].setAttrCSS(attr);
*/
	  } // for
     }
friseChapitre.showEvt=function()
	{
	var bandeRef=this.gestBandes.bandesData.Semaines;
	for (var evtNu=0;evtNu<dijonPraxisEvt.length;evtNu++){
		var EvtNu=bandeRef.evtAdd(
			dijonPraxisEvt[evtNu]
			,{"classe": "evt_ponctuel", "opacity": 1,"zIndex": 1005}
			,{
//				 deb: function(t){showActivity(global_Tfrise_msg,'add')}
//				,fin: function(t){showActivity(global_Tfrise_msg,'add')}
			 }
			);
	}
	}


// -- affichage de section de la frise -- //
friseChapitre.showActe=function(snNo){
	if(!snNo)return null;
	
	}
// ** a integrer dans timeline.js ** // 
// -- retourne la date actuelle sous forme d'evt -- //
friseChapitre.evtNow=function(){
	var d=new Date();
	return {"date_deb":{"unite":"jr","an":d.getFullYear(),"ms":d.getMonth()+1,"jr":d.getDate(),"hr":d.getHours(),"mn":d.getMinutes()},"titre":"now","title":"en ce moment","texte":""};
	}
// -- affichage de l'evenement: date du jours -- //
friseChapitre.showNow=function(){
	//return null;
	var bandeRef=this.gestBandes.bandesData.Semaines;
	var EvtNu=bandeRef.evtAdd(
		this.evtNow()
		,{"classe": "evt_ponctuel", "opacity": 1,"zIndex": 1100}
		,{
//			 deb: function(t){showActivity(global_Tfrise_msg,'add')}
//			,fin: function(t){showActivity(global_Tfrise_msg,'add')}
		}
		);
	}

//initialisation
//friseChapitre.styleInit();

}	//createFriseChapitre
