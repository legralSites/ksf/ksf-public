/*!
*/

friseBG=null;
function createFriseBG()
{
friseBG=new Tfrise({
	'idHTML':'friseBG_support'
	,'supportRegles':'friseBG_supportRegles'
	,'supportDatas':'friseBG_supportDatas'
	,'bandeModele':{'date_deb': {"an": 2014,"ms":1,"jr":1,'unite':'jr'},"date_fin": {"an": 2014,"ms":11,"jr":31,'unite':'jr'},'left_marge': 0,'longueur': 1000,'top': 0,'hauteur': 10}
	});


// -- affichage de section de la frise -- //
friseBG.showActe=function(snNo){
	if(!snNo)return null;
	
	}
// ** a integrer dans timeline.js ** // 
// -- retourne la date actuelle sous forme d'evt -- //
friseBG.evtNow=function(){
	var d=new Date();
	return {"date_deb":{"unite":"jr","an":d.getFullYear(),"ms":d.getMonth()+1,"jr":d.getDate(),"hr":d.getHours(),"mn":d.getMinutes()},"titre":"now","title":"en ce moment","texte":""};
	}
// -- affichage de l'evenement: date du jours -- //
friseBG.showNow=function(){
	//return null;
	var bandeRef=this.gestBandes.bandesData.Semaines;
	var EvtNu=bandeRef.evtAdd(
		this.evtNow()
		,{"classe": "evt_ponctuel", "opacity": 1,"zIndex": 1100}
		,{
//			 deb: function(t){showActivity(global_Tfrise_msg,'add')}
//			,fin: function(t){showActivity(global_Tfrise_msg,'add')}
		}
		);
	}


friseBG.styleInit=function()
	{
	}


friseBG.auto=function()
	{
	this.creer_regleA();
//	this.creerReperesAuto({'callbacks':{'texte':'Million','title':'details'}});
	this.creerReperesAuto({"repereNb":5});

	this.creer_bandes();

	this.showNow();
	this.showEvt();
//	this.showNow();
	}

friseBG.creer_regleA=function()
	{
	var options=undefined;
	this.gestBandes.creerBande({"genre": "regle",idHTML_parent:this.supportRegles, "idHTML": "regleA","nom":"regleA","top":0,"hauteur":30,"title": ""},options);
	}

friseBG.creer_bandes=function()
	{
	var _top=-10,options=undefined;

	_top+=10;this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeChapitres","nom":"Chapitres","top": _top,"hauteur":10,"title": "chapitres"},options);
	_top+=10;this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeActes","nom":"Actes","top": _top,"hauteur":10,"title": "actes"},options);
	_top+=10;this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeSemaines","nom":"Semaines","top": _top,"hauteur":10,"title": "semaines"},options);

	// - article parut- //
	_top+=10;this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeArticles","nom":"Articles","top": _top,"hauteur":10,"title": "Articles"},options);

	// - - //
	_top+=10;this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeHstorique","nom":"Historique","top": _top,"hauteur":10,"title": "événement Historique"},options);

	// - histoire et evevements generaux des groupes - //
	_top+=10;this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeCamrillia","nom":"Camarillia","top": _top,"hauteur":10,"title": "Camarilla"},options);

	_top+=10;this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeAnarch","nom":"Anarch","top": _top,"hauteur":10,"title": "Anarch"},options);

	_top+=10;this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeSabbat","nom":"Sabbat","top": _top,"hauteur":10,"title": "Sabbat"},options);


	// - villes - //
	_top+=10;this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeBerlin","nom":"Berlin","top": _top,"hauteur":10,"title": "Berlin"},options);
	_top+=10;this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeLondre","nom":"Londre","top": _top,"hauteur":10,"title": "Londre"},options);
	_top+=10;options=undefined;
	this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeLosAngeles","nom":"LosAngeles","top": _top,"hauteur":10,"title": "LosAngeles"},options);
	_top+=10;options=undefined;
	this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeDijon","nom":"Dijon","top": _top,"hauteur":10,"title": "Dijon"},options);

	// - personnages - //

	// - personnage: gangrel - //
	_top+=10;options=undefined;
	this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeGaebolg","nom":"Gaebolg","top": _top,"hauteur":10,"title": "Gaebolg"},options);
	_top+=10;options=undefined;
	this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeRavenBlack","nom":"RavenBlack","top": _top,"hauteur":10,"title": "RavenBlack"},options);
	_top+=10;options=undefined;
	this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeMatthewMacFerson","nom":"MatthewMacFerson","top": _top,"hauteur":10,"title": "MacFerson"},options);
	_top+=10;options=undefined;
	this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeMilaMoroshka","nom":"MilaMoroshka","top": _top,"hauteur":10,"title": "MilaMoroshka"},options);
	_top+=10;options=undefined;
	this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeDereckClayton","nom":"DereckClayton","top": _top,"hauteur":10,"title": "DereckClayton"},options);

	_top+=10;options={"opacity":0.75,"classe":"rouge"};
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeShafBrunker','nom':'ShafBrunker',"top": _top,'hauteur':10,"title": "ShafBrunker"},options);

	// - personnage: Tremere - //
	_top+=10;options={};
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeJergenHassenweld','nom':'JergenHassenweld',"top": _top,'hauteur':10,"title": "JergenHassenweld:primo de l'ancienne Praxis de Dijon"},options);
	
	// - personnage: Anarch - //
	_top+=10;options={};
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeNasimBadrani','nom':'NasimBadrani',"top": _top,'hauteur':10,"title": "NasimBadrani"},options);
	_top+=10;options={};
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeAnneFretet','nom':'AnneFretet',"top": _top,'hauteur':10,"title": "AnneFretet"},options);
	_top+=10;options={};
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeThomasIngbart','nom':'ThomasIngbart',"top": _top,'hauteur':10,"title": "ThomasIngbart","texte":"ThomasIngbart"},options);
	_top+=10;options={};
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeGillianJohnson','nom':'GillianJohnson',"top": _top,'hauteur':10,"title": "GillianJohnson"},options);
	_top+=10;options={};
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandePaulLavalee','nom':'PaulLavalee',"top": _top,'hauteur':10,"title": "PaulLavalee"},options);
	_top+=10;options={};
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeAlphonseSarlant','nom':'AlphonseSarlant',"top": _top,'hauteur':10,"title": "AlphonseSarlant"},options);
	_top+=10;options={};
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeCharlesTurgot','nom':'CharlesTurgot',"top": _top,'hauteur':10,"title": "CharlesTurgot"},options);

/*
 * this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bande','nom':'',"top": _top,'hauteur':10,"title": ""},options);
	_top+=10;options={};
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bande','nom':'',"top": _top,'hauteur':10,"title": ""},options);
	_top+=10;options={};
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bande','nom':'',"top": _top,'hauteur':10,"title": ""},options);
	_top+=10;options={};
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bande','nom':'',"top": _top,'hauteur':10,"title": ""},options);
*/
	}
// == function d'ajout des evt ==//
friseBG.showEvt_Construct=function(nom,data,css,options){
	var c=(typeof(css=='oject'))?css:{};
	var o=(typeof(options=='oject'))?options:{};
	//var bandeRef=friseBG.gestBandes.bandesData.Historique;
	var bandeRef=this.gestBandes.bandesData[nom];
//	var data=vampireEvt;
	//for (var dataNu=0;dataNu<data.length;dataNu++){var evtNu=bandeRef.evtAdd(data[dataNu],{"classe": "evt_historique", "opacity": 1,"zIndex": 1005},{});}
	for (var dataNu=0;dataNu<data.length;dataNu++){var evtNu=bandeRef.evtAdd(data[dataNu],c,o);}
	}


friseBG.showEvt=function()
	{

	// - structurel - //
//	this.showEvt_Construct("Chapitres",dijonPraxisChapitre,{"classe": "evt_historique"});
	this.showEvt_Chapitres();
//	this.showEvt_Construct("Actes",ArticlesEvt,{"classe": "evt_historique"});
	this.showEvt_Actes();
//	this.showEvt_Construct("Semaines",ArticlesEvt,{"classe": "evt_historique"});
	this.showEvt_Semaines();

	this.showEvt_Construct("Articles",ArticlesEvt,{"classe": "evt_historique"});

	//this.showEvt_historique();
	this.showEvt_Construct("Historique",vampireEvt,{"classe": "evt_historique", "opacity": 1,"zIndex": 1005});

	this.showEvt_Construct("Anarch",AnarchEvt,{"classe": "evt_historique"});
	this.showEvt_Construct("Sabbat",SabbatEvt,{"classe": "evt_historique"});

	this.showEvt_Construct("Dijon",DijonEvt,{"classe": "evt_historique"});
	this.showEvt_Construct("LosAngeles",LosAngelesEvt,{"classe": "evt_historique"});
	this.showEvt_Construct("Londre",LondreEvt,{"classe": "evt_historique"});
	this.showEvt_Construct("Berlin",BerlinEvt,{"classe": "evt_historique"});

	// - personnages - //
	// - personnages:gangrel - //
	this.showEvt_Construct("ShafBrunker",ShafBrunkerEvt,{"classe": "evt_historique"});//this.showEvt_Shafbrunker();
	this.showEvt_Construct("MatthewMacFerson",MatthewMacFersonEvt,{"classe": "evt_historique"});

	// - personnages:Tremere - //
	this.showEvt_Construct("JergenHassenweld",JergenHassenweldEvt,{"classe": "evt_historique"});


	// - personnages: Anarch - //
	this.showEvt_Construct("PaulLavalee",PaulLavaleeEvt,{"classe": "evt_historique"});
	this.showEvt_Construct("AlphonseSarlant",AlphonseSarlantEvt,{"classe": "evt_historique"});
	this.showEvt_Construct("NasimBadrani",NasimBadraniEvt,{"classe": "evt_historique"});
	this.showEvt_Construct("ThomasIngbart",ThomasIngbartEvt,{"classe": "evt_historique"});
	this.showEvt_Construct("GillianJohnson",GillianJohnsonEvt,{"classe": "evt_historique"});
	this.showEvt_Construct("CharlesTurgot",CharlesTurgotEvt,{"classe": "evt_historique"});


	this.showEvt_Construct("",Evt,{"classe": "evt_historique"});
							
	}

friseBG.showEvt_Chapitres=function()
	{
	var bandeRef=this.gestBandes.bandesData.Chapitres;
	var EvtNu=bandeRef.evtAdd(
				dijonPraxisChapitre[1][0]
//				,{'classe': 'evt_'+niveau,'opacity': 1,'zIndex': 1005}
				,{'opacity': 1,'zIndex': 1005}
				,{
//					 deb: function(t){showActivity(global_Tfrise_msg,'add')}
//					,fin: function(t){showActivity(global_Tfrise_msg,'add')}
				 }
				);
	}

friseBG.showEvt_Actes=function()
	{
	var bandeRef=this.gestBandes.bandesData.Actes;
	for (var scNu=1;scNu<6;scNu++){
		var EvtNu=bandeRef.evtAdd(
			dijonPraxisChapitre[1][scNu][0]
			,{"classe": "evt_acte", "opacity": 1,"zIndex": 1005}
			,{
//				 deb: function(t){showActivity(global_Tfrise_msg,'add')}
//				,fin: function(t){showActivity(global_Tfrise_msg,'add')}
			 }
			);

		var attr={};
		// - action sur click - //
//		attr.click=function(e,scNu,smNu){
	//		this.limite(c[scNu][smNu].deb,c[scNu][smNu].fin);
		attr.click=function(e){
			alert("e= "+e+" e.target.id"+e.target.id+"\n" );
		      //	this.limite(c[scNu][smNu].deb,c[scNu][smNu].fin);
			}

		// - action sur double Click - //
		attr.dblclick=function(){
			alert("fonction non implementée");
			}

		// - application des evenements - //
		bandeRef.evt[EvtNu].setAttrCSS(attr);
		}
	}

friseBG.showEvt_Semaines=function()
     {
     var bandeRef=this.gestBandes.bandesData.Semaines;
     var scNu=1,smNu=1;
     var c=dijonPraxisChapitre[1];
     for (var scNu=1;scNu<6;scNu++)
     for (var smNu=1;smNu<5;smNu++){
	  if(typeof(c[scNu][smNu])!=='object')continue;
	  var evtNu=bandeRef.evtAdd(
	       c[scNu][smNu]
	       ,{"classe": "evt_semaine","opacity": 1,"zIndex": 1005}
	       ,{
//	            deb: function(t){showActivity(global_Tfrise_msg,'add')}
//		    ,fin: function(t){showActivity(global_Tfrise_msg,'add')}
	       }
	       );

	// --- ajout des clicks (faire des modif dans timelne.js --- //
//	var attr={};
//	attr.click=function(e,scNu,smNu){
//		this.limite(c[scNu][smNu].deb,c[scNu][smNu].fin);
//		}
//	bandeRef.evt[evtNu].setAttrCSS(attr);
//
	  } // for
     }



//initialisation
//friseBG.styleInit();

}	//createFriseChapitre
